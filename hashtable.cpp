#include "hashtable.h"

int hashtab_hash(string key) {
	string p = key;
    int h = p.length();
    return h % HASHTAB_SIZE;
}

struct Id_Table *Id_Table_Init (int level)
{
    struct Id_Table *Table = (struct Id_Table*) calloc (1, sizeof (struct Id_Table));
    Table->level = level;
    Table->next = NULL;
    Table->sizeTable = 0;
    Table->map = new unordered_map<string, struct listnode*>();
    return Table;
}

void Add_Size_Table(struct Id_Table *table)
{
    table->sizeTable +=8;
}

void hashtab_add(unordered_map<string, struct listnode*> *map, string key, int value, int baseType, int type) {
    listnode *node = new listnode;

    if (node != NULL) {
        node->key = key;
        node->value = value;
        node->size = 8;
        node->offset = 0;
        node->base_type = baseType;
        node->type = type;
    }
    (*map)[key] = node;
}

struct listnode *hashtab_lookup(Id_Table *table, string key) {
    int index;
    listnode *node;

    if (table == NULL) {
        return NULL;
    }

    if ((*table->map).find(key) != (*table->map).end()) {
        return (*table->map)[key];
    }
    return hashtab_lookup(table->next, key);
}

void hashtab_delete(listnode **hashtab, string key) {
    int index;
    listnode *p, *prev = NULL;

    index = hashtab_hash(key);
    for (p = hashtab[index]; p != NULL; p = p->next) {
        if (p->key == key){
            if (prev == NULL) {
                hashtab[index] = p->next;
            } else {
                prev->next = p->next;
            }
            free(p);
            return;
        }
        prev = p;
    }
}

void hashtab_setOffset(Id_Table *table, string key, int offset)
{
    listnode *node = hashtab_lookup(table, key);
    node->offset = offset;    
}
